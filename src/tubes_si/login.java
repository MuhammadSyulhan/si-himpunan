/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubes_si;

import db.ConnectionManager;
import halaman_awal.awal_admin;
import halaman_awal.awal_user;
import java.awt.Image;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class login extends javax.swing.JFrame {

    /**
     * Creates new form login
     */
    public login() {
        initComponents();
       
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        isiPassword = new javax.swing.JPasswordField();
        isiUsername = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        isiStatus = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        tombolLogin = new javax.swing.JButton();
        tombol_reset = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel3.setBackground(new java.awt.Color(255, 204, 204));

        isiPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                isiPasswordActionPerformed(evt);
            }
        });

        isiUsername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                isiUsernameActionPerformed(evt);
            }
        });

        jLabel1.setText("Username");

        jLabel2.setText("Password");

        isiStatus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "admin", "user" }));
        isiStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                isiStatusActionPerformed(evt);
            }
        });

        jButton1.setText("Exit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        tombolLogin.setText("Login");
        tombolLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolLoginActionPerformed(evt);
            }
        });

        tombol_reset.setText("Reset");
        tombol_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombol_resetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(isiStatus, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(isiPassword)
                    .addComponent(jLabel2)
                    .addComponent(isiUsername)
                    .addComponent(jLabel1)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(tombolLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tombol_reset, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(61, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(isiUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(isiPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(isiStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tombolLogin)
                    .addComponent(tombol_reset))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(62, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(255, 204, 255));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 278, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void isiUsernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_isiUsernameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_isiUsernameActionPerformed

    private void isiPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_isiPasswordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_isiPasswordActionPerformed

    private void isiStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_isiStatusActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_isiStatusActionPerformed

    private void tombol_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombol_resetActionPerformed
        this.isiUsername.setText(null);
        this.isiPassword.setText(null);
    }//GEN-LAST:event_tombol_resetActionPerformed

    private void tombolLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolLoginActionPerformed
        Login();
    }//GEN-LAST:event_tombolLoginActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
         System.exit(0);
    }//GEN-LAST:event_jButton1ActionPerformed

    public void  Login(){
        ConnectionManager conMan=new ConnectionManager();
        Connection conn= conMan.logOn();  
        try{  
            String sql = "select * from tbl_login LEFT JOIN tbl_anggota ON tbl_anggota.nim = tbl_login.username "
                        + "LEFT JOIN tbl_jabatan ON tbl_jabatan.id_jabatan = tbl_anggota.id_jabatan "
                        + "LEFT JOIN tbl_sekolah ON tbl_sekolah.id_sekolah = tbl_anggota.id_sekolah "
                        + "where tbl_login.username = '" + isiUsername.getText() + "' and tbl_login.status = '"+isiStatus.getSelectedItem().toString()+"'" ;  
            Statement st = conn.createStatement();  
            ResultSet rsLogin = st.executeQuery(sql);  
      
            rsLogin.next();  
            rsLogin.last(); //mengecek jumlah baris pada hasil query
                
            if (rsLogin.getRow()==1 && rsLogin.getString("status").equalsIgnoreCase("user")){      
                if(isiPassword.getText().equals(rsLogin.getString("password"))){
                    JOptionPane.showMessageDialog(rootPane, "Login Berhasil!");
                    String nim, nama, alamat, no_hp, email, angkatan, status, jabatan, sekolah;
                    nim = rsLogin.getString("tbl_anggota.nim");
                    nama = rsLogin.getString("tbl_anggota.nama");
                    alamat = rsLogin.getString("tbl_anggota.alamat");
                    no_hp = rsLogin.getString("tbl_anggota.no_hp");
                    email = rsLogin.getString("tbl_anggota.email");
                    angkatan = rsLogin.getString("tbl_anggota.angkatan");
                    status = rsLogin.getString("tbl_anggota.status");
                    jabatan = rsLogin.getString("tbl_jabatan.nama");
                    sekolah = rsLogin.getString("tbl_sekolah.nama");
                    new awal_user(nim,nama,alamat,no_hp,email,angkatan,status,jabatan,sekolah).setVisible(true);   
                    this.dispose();               
                }
                else{
                    JOptionPane.showMessageDialog(rootPane, "Login Gagal, Silahkan Coba Lagi");  
                    isiUsername.requestFocus();
                    isiPassword.setText("");             
                }
            }
            
            else if (rsLogin.getRow()==1 && rsLogin.getString("status").equalsIgnoreCase("admin")){
                if(isiPassword.getText().equals(rsLogin.getString("password"))){
                    JOptionPane.showMessageDialog(rootPane, "Login Berhasil!");
                    new awal_admin().setVisible(true);
                    this.dispose();
                }
                else{
                    JOptionPane.showMessageDialog(rootPane, "Login Gagal, Silahkan Coba Lagi");  
                    isiUsername.requestFocus();
                    isiPassword.setText("");  
                }                   
            }
            
            else{  
                JOptionPane.showMessageDialog(rootPane, "Login Gagal, Silahkan Coba Lagi");  
                isiUsername.setText("");  
                isiPassword.setText("");  
                isiUsername.requestFocus();  
            }
    
        } 
        catch (SQLException e){  
            JOptionPane.showMessageDialog(rootPane,"Koneksi Gagal");
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField isiPassword;
    private javax.swing.JComboBox isiStatus;
    private javax.swing.JTextField isiUsername;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JButton tombolLogin;
    private javax.swing.JButton tombol_reset;
    // End of variables declaration//GEN-END:variables
}
